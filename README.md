# Type-safe FizzBuzz using Monoids and the Option type #

This repo is inspired by the original Haskell version at https://web.archive.org/web/20130511210903/http://dave.fayr.am/posts/2012-10-4-finding-fizzbuzz.html that was ported to Rust through the help of http://www.reddit.com/r/rust and Rust IRC channel.