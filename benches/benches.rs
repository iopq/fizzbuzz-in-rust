#[macro_use]
extern crate bencher;

use bencher::Bencher;
use fizzbuzz::cowbuzz;
use fizzbuzz::fizzbuzz;

fn bench_fizzbuzz(b: &mut Bencher) {
    b.iter(|| {
        for i in 1..=15 {
            fizzbuzz(&[("Fizz", &|i| i % 3 == 0), ("Buzz", &|i| i % 5 == 0)], i);
        }
    });
}

fn bench_cowbuzz(b: &mut Bencher) {
    b.iter(|| {
        for i in 1..=15 {
            cowbuzz(&[("Fizz", &|i| i % 3 == 0), ("Buzz", &|i| i % 5 == 0)], i);
        }
    });
}

benchmark_group!(benches, bench_fizzbuzz, bench_cowbuzz);
benchmark_main!(benches);
