use fizzbuzz;

use failure::Error;
use std::io::stdout;
use std::io::BufWriter;
use std::io::Write;
use std::str::FromStr;

fn main() -> Result<(), Error> {
    let n: i32 = std::env::args()
        .nth(1)
        .map(parse)
        .unwrap_or(Ok(100))?;

    let stdout = stdout();
    let mut sink = BufWriter::new(stdout.lock());

    for i in 1..=n {
        sink.write(
            fizzbuzz::cowbuzz(
                &[
                    ("Fizz", &|i| i % 3 == 0),
                    ("Buzz", &|i| i % 5 == 0),
                    ("Bazz", &|i| i % 7 == 0),
                ],
                i,
            )
            .as_bytes(),
        )?;
        sink.write(b"\n")?;
    }

    Ok(())
}

//type bridging
fn parse(input: String) -> Result<i32, Error> {
    Ok(i32::from_str(&input)?)
}
